function binarySearch (arr, item) {
    let start = 0
    let end = arr.length - 1

    while (end >= start) {
        let mid = Math.floor((end - start)/2) + start

        if (arr[mid] === item) {
            return mid
        } else if (arr[mid] > item) {
            end = mid - 1
        } else {
            start = mid + 1
        }
    }

    return -1
}

function main () {
    let arr = [1, 3, 5, 6, 7, 10]
    let i = binarySearch(arr, 4)
    console.log(i)
}

main()
