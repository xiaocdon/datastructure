class BST {
    constructor () {
        this.root = null
    }

    insert (key, value) {
        if (!this.root) {
            this.root = new BST.Node(key, value)
            return
        }

        this._insert(this.root, key, value)
    }

    insertUnRecursive (key, value) {
        if (!this.root) {
            this.root = new BST.Node(key, value)
            return
        }

        let current = this.root

        while (current) {
            if (current.key > key) {
                if (current.left) {
                    current = current.left
                } else {
                    current.left = new BST.Node(key, value)
                    break
                }
            } else if (current.key < key) {
                if (current.right) {
                    current = current.right
                } else {
                    current.right = new BST.Node(key, value)
                    break
                }
            } else {
                current.value = value
                break
            }
        }
    }

    _insert (root, key, value) {
        if (root.key > key) {
            if (root.left) {
                this._insert(root.left, key, value)
            } else {
                root.left = new BST.Node(key, value)
            }
        } else {
            if (root.right) {
                this._insert(root.right, key, value)
            } else {
                root.right = new BST.Node(key, value)
            }
        }
    }

    get (key) {
        if (!this.root) {
            return null
        }

        let current = this.root

        while (current) {
            if (current.key === key) {
                return current.value
            } else if (current.key > key) {
                current = current.left
            } else {
                current = current.right
            }
        }

        return null
    }

    visit () {
        this._visit(this.root)
    }

    _visit (root) {
        if (!root) {
            return
        }

        this._visit(root.left)
        console.log(root.key)
        this._visit(root.right)
    }

    levelOrder () {
        if (!this.root) {
            return
        }

        let queue = new Array()
        queue.push(this.root)

        let current
        while (queue.length) {
            current = queue.shift()
            console.log(current.key)
            if (current.left)
                queue.push(current.left)
            
            if (current.right)
                queue.push(current.right)
        }
    }

    delete (key) {

    }

    _delete (root, key) {
        if (!root) {
            return
        }

        if (root.key < key) {
            root.right = this._delete(root.right, key)
        } else if (root.key > key) {
            root.left = this._delete(root.left, key)
        } else {
            if (!root.right) {
                return root.left
            }

            let minNode = this.findMin(root.right)
            root.right = this._removeMin(root.right)
            minNode.left = root.left
            minNode.right = root.right

            return minNode
        }
    }

    findMin (root) {
        if (!root.left) {
            return root
        }

        return this.findMin(root.left)
    }

    /**
     * Removes the min item from the BST in non-recursive way
     */
    removeMinNonRecursive () {
        if (!this.root) {
            return
        }

        let current = this.root
        let parent
        
        while (true) {
            if (current.left) {
                parent = current
                current = current.left
            } else {
                if (!parent) {
                    this.root = current.right
                } else {
                    parent.left = current.right
                }
                return current.key
            }
        }
    }

    removeMin () {
        if (!this.root) {
            return
        }

        this.root = this._removeMin(this.root)
    }

    /**
     * Removes the min node from sub-tree and return the root of the updated one
     * @param {BST.Node} root 
     */
    _removeMin (root) {
        if (root.left) {
            root.left = this._removeMin(root.left)
            return root
        } else {
            return root.right
        }
    }

    removeMax () {
        if (!this.root) {
            return
        }

        this.root = this._removeMax(this.root)
    }

    /**
     * Removes the max node from sub-tree and return the root of the updated one
     * @param {BST.Node} root 
     */
    _removeMax (root) {
        if (root.right) {
            root.right = this._removeMax(root.right)
            return root
        } else {
            return root.left
        }
    }


    /**
     * Removes the max item from the BST in non-recursive way
     */
    removeMaxNonRecursive () {
        if (!this.root) {
            return
        }

        let current = this.root
        let parent

        while (true) {
            if (current.right) {
                parent = current
                current = current.right
            } else {
                if (!parent) {
                    this.root = current.left
                } else {
                    parent.right = current.left
                }
                return current.key
            }
        }
    }
}

BST.Node = class {
    constructor (key, value) {
        this.key = key
        this.value = value

        this.left = null
        this.right = null
    }
}

const tree = new BST()
tree.insertUnRecursive(10, 1)
tree.insertUnRecursive(11, 10)
tree.insertUnRecursive(1, 5)
tree.insertUnRecursive(8, 100)
tree.insertUnRecursive(8, 1)

tree.levelOrder()
console.log('remove')
tree.removeMax()
tree.levelOrder()
tree.removeMax()
console.log('remove')
tree.levelOrder()
tree.removeMax()
console.log('remove')
tree.levelOrder()
tree.removeMax()
console.log('remove')
tree.levelOrder()

// tree.visit()


// console.log(tree.get(11))
// console.log(tree.get(8))
// console.log(tree.get(1111))