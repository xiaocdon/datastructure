class UnionFind {
    constructor (n) {
        this.unionIds = new Array(n)
        for (let i = 0; i < n; i++) {
            this.unionIds[i] = i
        }
    }

    /**
     * Get p unionId from the array
     * @param {*} p 
     */
    find (p) {
        return this.unionIds[p]
    }

    isConnected (p, q) {
        return this.unionIds[p] === this.unionIds[q]
    }

    connect (p, q) {
        const pUnionId = this.unionIds[p]
        const qUnionId = this.unionIds[q]

        this.unionIds = this.unionIds.map(id => id === pUnionId ? qUnionId : id)
    }
}

const unionFind = new UnionFind(10)
unionFind.connect(0, 9)
unionFind.connect(0, 7)
unionFind.connect(8, 7)

console.log(unionFind.isConnected(0, 9))
console.log(unionFind.isConnected(8, 9))
