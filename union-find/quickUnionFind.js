class QuickUnionFind {
    constructor (n) {
        this.parentIds = new Array(n).fill(null).map((_, i) => i)
    }

    /**
     * Finds the root id of i with path compression optimization
     * @param {*} i 
     */
    findRoot (i) {
        if (this.parentIds[i] === i) {
            return i
        }

        this.parentIds[i] = this.findRoot(this.parentIds[i])

        return this.parentIds[i]
    }

    /**
     * Connects i and j
     * @param {*} i 
     * @param {*} j 
     */
    connect (i, j) {
        const iRoot = this.findRoot(i)
        const jRoot = this.findRoot(j)

        this.parentIds[jRoot] = iRoot
    }

    /**
     * Determines if i and j is connected
     */
    isConnected (i, j) {
        return this.findRoot(i) === this.findRoot(j)
    }
}

const unionFind = new QuickUnionFind(10)
unionFind.connect(0, 9)
unionFind.connect(0, 7)
unionFind.connect(8, 7)
unionFind.connect(5, 0)

console.log(unionFind.isConnected(0, 9))
console.log(unionFind.isConnected(8, 9))
console.log(unionFind.isConnected(5, 9))
console.log(unionFind.isConnected(5, 3))
console.log(unionFind.isConnected(0, 7))
