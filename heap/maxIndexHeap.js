class MaxIndexHeap {
    constructor (cap) {
        this.cap = cap
        this.size = 0
        this.data = new Array(cap + 1)
        this.indexes = new Array(cap + 1)
        this.reverse = new Array(cap + 1)

        for (let i = 1; i <= this.cap + 1; i++) {
            this.reverse[i] = 0
        }
    }

    /**
    Inserts item to i position of original array
    */
    insert (i, item) {
        debugger
        i++
        if (this.reverse[i] !== 0) {
            throw new Error(`Can't not fullfill insertion opt for existing data in position ${i}`)
        }

        if (this.size > this.cap) {
            throw new Error("Can't not fullfill inserion opt for maximum number of heap size has reached")
        }

        this.data[i] = item
        this.indexes[this.size + 1] = i
        this.reverse[i] = this.size + 1
        this.shiftUp(this.size + 1)

        this.size++
    }

    /**
    Shift up the item in position i of the array
     */
    shiftUp (i) {
        while (i > 1 && this.data[this.indexes[Math.floor(i/2)]] < this.data[this.indexes[i]]) {
            this.swap(i, Math.floor(i/2))
            i = Math.floor(i/2)
        }
    }

    swap (i, j) {
        let temp = this.indexes[i]
        this.indexes[i] = this.indexes[j]
        this.indexes[j] = temp

        this.reverse[this.indexes[i]] = i
        this.reverse[this.indexes[j]] = j
    }

    /**
    Get the max item from the heap, the data should not be visisted via this heap api any more
    */
    extractMax () {
        if (this.size === 0) {
            throw new Error("Empty error")
        }

        let item = this.data[this.indexes[1]]

        this.swap(1, this.size)
        this.reverse[this.indexes[this.size]] = 0
        this.size--

        this.shiftDown(1)

        return item
    }

    /**
    Changes the data in the i position of data array
     */
    change (i, item) {
        i++
        if (this.reverse[i] === 0) {
            throw new Error("No item in pos i")
        }

        this.data[i] = item

        this.shiftUp(this.reverse[i])
        this.shiftDown(this.reverse[i])   
    }

    shiftDown (i) {
        if (i === this.size) {
            return
        }

        let leftChild = 2 * i
        let rightChild = leftChild + 1
        let maxChild = leftChild

        if (rightChild <= this.size && this.data[this.indexes[rightChild]] > this.data[this.indexes[leftChild]]) {
            maxChild = rightChild
        }

        if (this.data[this.indexes[maxChild]] > this.data[this.indexes[i]]) {
            this.swap(maxChild, i)
            this.shiftDown(maxChild)
        }
    }
}

function main () {
    let heap = new MaxIndexHeap(10)

    heap.insert(0, 10)
    heap.insert(1, 15)
    heap.insert(2, 1)
    heap.insert(3, 8)

    console.log(heap.extractMax())
    console.log(heap.extractMax())
    console.log(heap.extractMax())
    
    heap.insert(1, 100)
    heap.change(2, 1000)

    console.log(heap.extractMax())
    console.log(heap.extractMax())

    console.log(heap.extractMax())
}

main()
